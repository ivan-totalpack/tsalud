import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { LoginService} from '../../services/login.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {

  fecha!: any;
  hora!: string;
  botonSalir:boolean = true;

  constructor(
    private datePipe: DatePipe,
     private _loginService:LoginService,
     private router: Router
     ) { 
    this.fechaHora();
  }

  ngOnInit(): void {
    if( this.router.url == "/login"){
      this.botonSalir = false;
    }
   
  }

  fechaHora(): void {
    setInterval(() => {
      const currentDate = new Date();
      this.fecha = `${this.datePipe.transform(currentDate, 'dd/MM/yyyy')}`;
      this.hora = `${this.datePipe.transform(currentDate, 'HH:mm')}`;
    }, 1000);
  }

  salir(){
    this._loginService.cerrarSession();
  }

}
