import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'teclado_alfanumerico',
  templateUrl: './teclado-alfanumerico.component.html',
    styles: [
  ]
})
export class TecladoAlfanumericoComponent implements OnInit {

  @Output() tecleado = new EventEmitter<string>();
  add_teclas:string = "";

  constructor() { }

  ngOnInit(): void {
  }

  prees(tecla:string){
    if(tecla == '*') {
      const delet_tecla = this.add_teclas.substring(0, this.add_teclas.length - 1);
      this.add_teclas = delet_tecla;
      this.tecleado.emit(this.add_teclas);
    }else{
      this.add_teclas += tecla;
      this.tecleado.emit(this.add_teclas);
    }
  }

  limpiarTecleado(){
    this.add_teclas = "";
    this.tecleado.emit(this.add_teclas);
  }

}
