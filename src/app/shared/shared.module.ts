import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TecladoNumericoComponent } from './teclado-numerico/teclado-numerico.component';
import { TecladoAlfanumericoComponent } from './teclado-alfanumerico/teclado-alfanumerico.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    TecladoNumericoComponent,
    TecladoAlfanumericoComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    HeaderComponent,
    FooterComponent,
    TecladoNumericoComponent,
    TecladoAlfanumericoComponent
  ]
})
export class SharedModule { }
