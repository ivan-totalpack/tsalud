import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { PagesComponent } from './pages/pages.component';

const routes: Routes = [

  {
    path: '', 
    redirectTo: 'login', 
    pathMatch: 'full'},

  { 
    path:'login',
    component:LoginComponent,
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },

  { 
    path:'pages',
    component:PagesComponent,
    //canActivate:[LoginGuard],
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
