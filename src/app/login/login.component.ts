import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  ngOnInit(): void {
  }

  form!: FormGroup;
  tipo_form:number = 1;
  focus_rut:boolean = false;
  focus_pasaport:boolean = false;

  @ViewChild("teclado1") teclado1: any;
  @ViewChild("teclado2") teclado2: any;
  
  constructor(
    private fb: FormBuilder,
    private _loginService:LoginService
    ) { 
      this.tipoFom(1);
    }


  tipoFom(tipoform:number){
    this.tipo_form = tipoform;

    if(tipoform == 1){
      this.form = this.fb.group({
        rut:['', Validators.required],
      })
    }else{
      this.form = this.fb.group({
        pasaporte:['', Validators.required],
      })
    }

  }

  focus(tipo:number){
    if(tipo == 1){
      this.focus_rut = true;
      this.focus_pasaport = false;
    }else{
      this.focus_pasaport = true;
      this.focus_rut = false;
    }
  }

  tecleado(teclas:string){
    if(this.tipo_form == 1){
      this.form.value.rut = teclas;
    }else{
      this.form.value.pasaporte = teclas;
    }
  }

  enviarForm(){
    this._loginService.login(this.form.value)
  }

}
