import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

    usuario:any;

  constructor(private router:Router) { }

  estaLogeado():Boolean{
    if(localStorage.getItem('usuario')){
      return true
    }else{
      return false
    }
  }

  login(form:any){
     if(form.rut == '123456'){
        localStorage.setItem('usuario', JSON.stringify(form.rut))
        this.router.navigate(['/pages/menu'])
     }else if(form.pasaporte == 'ejp-12345'){
      localStorage.setItem('usuario', JSON.stringify(form.pasaporte))
      this.router.navigate(['/pages/menu'])
     }else{
       alert('error de autenticación')
     }
  }
  
  cerrarSession(){
    localStorage.clear();
    this.router.navigate(['/login'])
  }

  salir(){
    setInterval(() => {
      localStorage.clear();
      this.router.navigate(['/login'])
    }, 5000);
  }
}
