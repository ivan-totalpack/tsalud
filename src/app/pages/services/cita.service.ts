import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CitaService {

  constructor(private router:Router) { }

  consultaDoctores(): Observable<any[]>{
    const doctores = [
      {
        id:1, nombre:'RAFAEL JOSE CASTRO FINALDI',
        tipo: 'MEDICINA ADULTO', 
        hora:'14:50', 
        foto:'doctor2.png',
        pacientes:'5',
        costo: 54900
       },
      {
        id:2,
        nombre:'MARIA JOSE CASTILLO CID',
        tipo: 'NEUROLOGIA ADULTO', 
        hora:'16:30', 
        foto:'doctora.png',
        pacientes:'3',
        costo: 56000
      },
      {
        id:3,
        nombre:'CRISTIAN ANTONO ERAZO MARTINEZ', 
        tipo: 'PEDIATRIA', 
        hora:'15:20', 
        foto:'doctor2.png',
        pacientes:'8',
        costo: 45000
      },
      {
        id:4,
        nombre:'JOSE MARIA CASTRO FINALDI', 
        tipo: 'NEUROLOGIA ADULTO', 
        hora:'14:15', 
        foto:'doctora2.png',
        pacientes:'2',
        costo: 49500
       },
      {
        id:5,
        nombre:'RAFAEL JOSE CASTRO FINALDI', 
        tipo: 'MEDICINA ADULTO', 
        hora:'15:30', 
        foto:'doctor.png',
        pacientes:'4',
        costo: 60500
       },
        
      {
        id:6,
        nombre:'RAMON JOSE CASTILLO FINALDI', 
        tipo: 'MEDICINA ADULTO', 
        hora:'14:50', 
        foto:'doctora.png',
        pacientes:'3',
        costo: 40350
       }
    ];
      return of(doctores);
  }

  registroCita(costo:number){
    localStorage.setItem('doctor_costo', JSON.stringify(costo))
    this.router.navigate(['/pages/prevision']);
  }

  

}
