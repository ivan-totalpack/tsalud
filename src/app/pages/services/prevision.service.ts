import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PrevisionService {

  constructor(private router:Router) { }

  consultaPrevision(): Observable<any[]>{
    const prevision = [
      {
        id:1, 
        imagen:'bancamedica.png',
        aporte: 36900
       },
      {
        id:2,
        imagen:'consalud.png',
        aporte: 12000
      },
      {
        id:3,
        imagen:'cruzblanca.jpg',
        aporte: 25640
      },
      {
        id:4,
        imagen:'vidatres.png',
        aporte: 30640
      },
      {
        id:5,
        imagen:'masvida.png',
        aporte: 30000
      },
      {
        id:6,
        imagen:'colmena.png',
        aporte: 32000
      },
      {
        id:7,
        imagen:'fonasa.jpg',
        aporte: 10000
      },
      
    ];
      return of(prevision);
  }

  registrarPrevision(aporte:number){
    localStorage.setItem('prevision', JSON.stringify(aporte))
    this.router.navigate(['/pages/detalle-pago']);
  }

}
