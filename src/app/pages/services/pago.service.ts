import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PagoService {

  constructor(private router:Router) { }

  registroPago(){
    this.router.navigate(['/pages/metodo-pago']);
  }

  pagarApi(){
    this.router.navigate(['/pages/proceso-pago']);
  }

  pagarCaja(){
    this.router.navigate(['/pages/tiket']);
  }

  pagoDetalle(){
    let costo = localStorage.getItem('doctor_costo');
    let prevision = localStorage.getItem('prevision');
    let detalle = {costo:costo,prevision:prevision} 
    return of(detalle);
  }

}
