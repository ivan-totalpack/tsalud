import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { MenuSelectorComponent } from './components/menu-selector/menu-selector.component';
import { CitaMedicaComponent } from './components/cita-medica/cita-medica.component';
import { AtencionComponent } from './components/atencion/atencion.component';
import { PrevisionComponent } from './components/prevision/prevision.component';
import { PagoDetalleComponent } from './components/pago-detalle/pago-detalle.component';
import { PagoMetodoComponent } from './components/pago-metodo/pago-metodo.component';
import { PagoProcesoComponent } from './components/pago-proceso/pago-proceso.component';
import { PagoExitosoComponent } from './components/pago-exitoso/pago-exitoso.component';
import { TiketComponent } from './components/tiket/tiket.component';
import { EnviarCorreoComponent } from './components/enviar-correo/enviar-correo.component';



@NgModule({
  declarations: [
    PagesComponent,
    MenuSelectorComponent,
    CitaMedicaComponent,
    AtencionComponent,
    PrevisionComponent,
    PagoDetalleComponent,
    PagoMetodoComponent,
    PagoProcesoComponent,
    PagoExitosoComponent,
    TiketComponent,
    EnviarCorreoComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
