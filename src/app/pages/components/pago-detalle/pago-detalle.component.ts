import { Component, OnInit } from '@angular/core';
import { PagoService } from '../../services/pago.service'

@Component({
  selector: 'app-pago-detalle',
  templateUrl: './pago-detalle.component.html',
  styles: [
  ]
})
export class PagoDetalleComponent implements OnInit {

  costo:any;
  prevision:any;

  constructor(private _pagoService:PagoService) { }

  ngOnInit(): void {
    this.obtnerCosto();
  }

  obtnerCosto(){
    this._pagoService.pagoDetalle()
      .subscribe(res => {
        this.costo = res.costo;
        this.prevision = res.prevision;
      })
  }
  
  registrarPago(){
    this._pagoService.registroPago();
  }
}
