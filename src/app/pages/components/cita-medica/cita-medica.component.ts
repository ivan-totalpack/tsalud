import { Component, OnInit } from '@angular/core';
import { CitaService } from '../../services/cita.service'
import { Router } from '@angular/router'

declare var window: any;
@Component({
  selector: 'app-cita-medica',
  templateUrl: './cita-medica.component.html',
  styles: [
  ]
})
export class CitaMedicaComponent implements OnInit {

  doctores:any= [];
  modal: any;
  doctor_nombre!:string;
  doctor_pacientes!:number;
  doctor_costo!:any;

  constructor(
    private _citaService:CitaService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.listarDoctores();

    this.modal = new window.bootstrap.Modal(
      document.getElementById('exampleModal')
    );

  }
  
  listarDoctores(){
    this._citaService.consultaDoctores()
      .subscribe(res => {
        this.doctores = res;
      })
  }

  openModal(nombre:string,pacientes:number,costo:string) {
    this.doctor_nombre = nombre;
    this.doctor_pacientes = pacientes;
    this.doctor_costo = costo;
    this.modal.show();
  }

  regitrarConsulta(){
    this.modal.hide();
    this._citaService.registroCita(this.doctor_costo);
  }

  tiket(){
    this.modal.hide();
    this.router.navigate(['/pages/tiket']);
  }

}
