import { Component, OnInit } from '@angular/core';
import { PagoService } from '../../services/pago.service'

@Component({
  selector: 'app-pago-metodo',
  templateUrl: './pago-metodo.component.html',
  styles: [
  ]
})
export class PagoMetodoComponent implements OnInit {

  constructor(private _pagoService:PagoService) { }

  ngOnInit(): void {
  }

  pagoApi(){
    this._pagoService.pagarApi();
  }

  pagoCaja(){
    this._pagoService.pagarCaja();
  }

}
