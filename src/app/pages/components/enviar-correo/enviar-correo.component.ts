import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-enviar-correo',
  templateUrl: './enviar-correo.component.html',
  styles: [
  ]
})
export class EnviarCorreoComponent implements OnInit {

  formCorreo = new FormGroup({
    correo: new FormControl('t-salud@gmail.com',Validators.required),
  });

  submit:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  tecleado(teclas:string){
      this.formCorreo.value.correo = teclas;
  }

  enviarCorreo(){
    if(!this.formCorreo.invalid){
      this.submit = true;
      this.formCorreo.value;
    }else{
      this.submit = false;
    }
   
  }

}
