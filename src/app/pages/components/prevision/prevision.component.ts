import { Component, OnInit } from '@angular/core';
import { PrevisionService } from '../../services/prevision.service'

@Component({
  selector: 'app-prevision',
  templateUrl: './prevision.component.html',
  styles: [
    
  ]
})
export class PrevisionComponent implements OnInit {
  prevision_array:any = [];

  constructor(private _previsionService:PrevisionService,) { }

  ngOnInit(): void {
    this.listarPrevision();
  }

  listarPrevision(){
    this._previsionService.consultaPrevision()
      .subscribe(res => {
        this.prevision_array = res;
      })
  }

  registrarPrevision(aporte:number){
    this._previsionService.registrarPrevision(aporte);
  }

}
