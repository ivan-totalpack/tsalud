import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginGuard } from "../guards/login.guard";

import { MenuSelectorComponent } from './components/menu-selector/menu-selector.component';
import { CitaMedicaComponent } from './components/cita-medica/cita-medica.component';
import { AtencionComponent } from './components/atencion/atencion.component';
import { PrevisionComponent } from './components/prevision/prevision.component';
import { PagoDetalleComponent } from './components/pago-detalle/pago-detalle.component';
import { PagoMetodoComponent } from './components/pago-metodo/pago-metodo.component';
import { PagoProcesoComponent } from './components/pago-proceso/pago-proceso.component';
import { PagoExitosoComponent } from './components/pago-exitoso/pago-exitoso.component';
import { TiketComponent } from './components/tiket/tiket.component';
import { EnviarCorreoComponent } from './components/enviar-correo/enviar-correo.component';

const routes: Routes = [

  { path:'menu', component: MenuSelectorComponent, canActivate:[LoginGuard] },
  { path:'cita', component: CitaMedicaComponent, canActivate:[LoginGuard] },
  { path:'atencion', component: AtencionComponent, canActivate:[LoginGuard] },
  { path:'prevision', component: PrevisionComponent, canActivate:[LoginGuard] },
  { path:'detalle-pago', component: PagoDetalleComponent, canActivate:[LoginGuard] },
  { path:'metodo-pago', component: PagoMetodoComponent, canActivate:[LoginGuard] },
  { path:'proceso-pago', component: PagoProcesoComponent, canActivate:[LoginGuard] },
  { path:'pago-exito', component: PagoExitosoComponent, canActivate:[LoginGuard] },
  { path:'tiket', component: TiketComponent, canActivate:[LoginGuard] },
  { path:'envair-correo', component: EnviarCorreoComponent, canActivate:[LoginGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
